# Context Active Inspector

Toolbar item that provides inspector of active contexts and navigation within.

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/context_active_inspector).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

Context module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Set permission `access context active inspector` for admin users.


## Maintainers

- Vlad Moiseienko - [vlad.dancer](https://www.drupal.org/u/vladdancer)
- Kateryna Karpenko - [KateKarpenko](https://www.drupal.org/u/katekarpenko)
